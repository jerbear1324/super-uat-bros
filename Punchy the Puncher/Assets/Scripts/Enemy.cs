﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	private Transform tf;
	public bool moveLeft;
	public bool moveRight;
	public float speed;
	public float moveTimer;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform>();
		StartCoroutine (MoveLeft());
	}
	
	// Update is called once per frame
	void Update () {
		if (moveLeft == true) {
			tf.position -= tf.right * speed;
		}
		if (moveRight == true) {
			tf.position += tf.right * speed;
		}
		
	}
	IEnumerator MoveLeft () {
		moveLeft = true;
		moveRight = false;
		yield return new WaitForSeconds (moveTimer);
		StartCoroutine (MoveRight());
	}
	IEnumerator MoveRight () {
		moveLeft = false;
		moveRight = true;
		yield return new WaitForSeconds (moveTimer);
		StartCoroutine (MoveLeft());
	}
}
