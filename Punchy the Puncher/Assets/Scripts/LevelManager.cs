﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	void Start (){
	}

	public void LevelSelection () {
		SceneManager.LoadScene("LevelSelection", LoadSceneMode.Single);
	}

	public void StartScreen (){
		SceneManager.LoadScene("StartScreen", LoadSceneMode.Single);
	}
	public void OneTacOne (){
		SceneManager.LoadScene("1-1", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void OneTacTwo (){
		SceneManager.LoadScene("1-2", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void OneTacThree (){
		SceneManager.LoadScene("1-3", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void TwoTacOne (){
		SceneManager.LoadScene("2-1", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void TwoTacTwo (){
		SceneManager.LoadScene("2-2", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void TwoTacThree (){
		SceneManager.LoadScene("2-3", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void ThreeTacOne (){
		SceneManager.LoadScene("3-1", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void ThreeTacTwo (){
		SceneManager.LoadScene("3-2", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}

	public void ThreeTacThree (){
		SceneManager.LoadScene("3-3", LoadSceneMode.Single);
		Physics2D.gravity = new Vector2 (0f, -9.8f);
	}
	public void QuitGame (){
		Application.Quit ();
	}
}
