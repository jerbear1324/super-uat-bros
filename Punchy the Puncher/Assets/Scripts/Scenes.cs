﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenes: MonoBehaviour {
	void Start (){
	}
	// Use this for initialization
	public void MainMenu () {
		SceneManager.LoadScene("MainMenu");
	}
	public void LevelSelection () {
		SceneManager.LoadScene("LevelSelection");
	}
	public void OneTacOne ()
	{
		SceneManager.LoadScene ("1-1");
	}
	public void OneTacTwo ()
	{
		SceneManager.LoadScene ("1-2");
	}
	public void OneTacThree ()
	{
		SceneManager.LoadScene ("1-3");
	}
	public void QuitGame ()
	{
		Application.Quit ();
	}
}