﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {
	public string TagList = "|Player|"; //Who can teleport
	private Transform tf;
	public Transform Destination; //Teleport Location



	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.R))
			tf.transform.position = Destination.transform.position;
		Debug.Log ("Teleport Me");
		
	}
}
