﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float speed;
    public float jumpHeight;
	private Transform tf;
	public bool isJumping = false;
	//Set Jumping as False to start
	public bool isWalking = false;
	//Trying to fix the walking animation bug (FIXED)
	public bool isGrounded = true;
	private Animator anim;
	private SpriteRenderer sr;
	public Transform Destination; //Teleport Location
	public KeyCode jumpKey;
	public KeyCode rightKey;
	public KeyCode leftKey;
	//Allowing for change of keys if necessary
	public AnimationClip Idle;
	public AnimationClip Walking;
	public AnimationClip Jumping;
	//Animation Clips
	//RaycastHit2D = Physics2D.Raycast (transform.position, -Vector2.up, 0.1);
	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform>();
		sr = GetComponent<SpriteRenderer> ();
		anim = GetComponent<Animator> ();

	}

	// Update is called once per frame
	void Update () {

		//if (landHo != null) {
		//	isGrounded = true;
		//}
		if (isWalking == false) {
			if (isJumping == false) {
				isGrounded = true;
				anim.Play (Idle.name);
			}
		}
		//WALKING---------------------------------------------------
		if (Input.GetKey (rightKey)) {
			isWalking = true;
			tf.position += tf.right * speed;
			sr.flipX = false;
			//Walk Right
		}
		if (Input.GetKey (leftKey)) {
			isWalking = true;
			tf.position -= tf.right * speed;
			sr.flipX = true;
		}
			//Walk Left
		if (isWalking == true) {
			if (isJumping == false) {
				anim.Play (Walking.name);
			}
			//Play Walking Animation when key is pressed
		}
		if (Input.GetKeyUp (leftKey)) {
			isWalking = false;
		}
		if (Input.GetKeyUp (rightKey)) {
			isWalking = false;
		}
		//WALKING----------------------------------------------------

		//Jumping----------------------------------------------------
		if (Input.GetKeyDown(jumpKey)) {
            isJumping = true;
			isGrounded = false;
		}
		
        if (isJumping == true)
        {
            tf.position += tf.up * jumpHeight;
			//Jump Height
			anim.Play (Jumping.name);
        }
		if (Input.GetKeyUp (jumpKey)) {
			isJumping = false;
		}
		//Jumping----------------------------------------------------
		if (Input.GetKey (KeyCode.S)) {
			//Duck
		}
		if (Input.GetKey (KeyCode.R))
			tf.transform.position = Destination.transform.position;
		//Teleports player to Destination
	}
}
